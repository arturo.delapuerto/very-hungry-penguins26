

exception Pascettedirection (*ce manchot ne peut pas aller par là*)
exception Peutpasbouger     (* ce manchot ne peut pas bouger*)
exception FatalError of string (* autre erreur, non rattrapée par le code*)


(*Return the number of fish on the case position*)

let nb_poissons map position =
  (*let grid = state#get_map () in (* TODO it is better to use get_cell*)*)
  let (x,y)=position in
  match (map.(x).(y)) with (*ATTENTION confusion abscisse/ordonnées possible*)
  | Move.ICE n -> n
  | Move.TELEPORT(dest, false) -> 0
  | Move.TELEPORT(dest, true) ->raise (FatalError "Ceci n'est pas censé arriver, un téléporteur occupé est dans la liste")
  | Move.WATER ->raise (FatalError "Ceci n'est pas censé arriver, un case d'eau est dans la liste")
  | _ -> raise (FatalError "Ceci n'est pas censé arriver, un case non valide est dans la liste")



(*  Return the element of maximum value and its index
 * in a list of couple (position, value) *)
let max_list_pos tab =
  let index = ref (-1) in
  let k = ref (-1) in
  let max_value x y=
    index:= !index + 1;
    let _, b = x and _ ,d = y in
    if b>d then x else (k:= !index ; y)
  in
  let (pos,max)= List.fold_left max_value ((-1,-1),0) tab in
  (pos,max,!k)



(*Transforme an array of position into an array of couple
  * (position, value) where fct_value give the value of a position*)
let position_value list_pos fct_value=
  let aux p =
    (p, fct_value p)
  in
  List.map aux list_pos
(* pas forcément utile en fait*)




(* Function which compute the reachable position in direction d*)
let accessible map p d=
  let rec aux n =
    let (legal,_,_) = Move.legal_move_n map p (d,n) in
    if legal
    then (Move.move_n map p (d,n))::(aux (n+1))
    else []
  in
  let l = aux 1 in
  if l=[] then raise Pascettedirection;
  l




(*note that the distance of a movement to reach a position
  is index of the position in the list +1 *)

(* Functions which return the best possible move in direction d from position p
 * where the value of a move is given by fct_value*)
let best_of_dir map p fct_value d=
  let l = accessible map p d in
  let l2 = position_value l fct_value in
  let (p',max,index) = max_list_pos l2 in
  ((d,index+1), max)
(* (p, index+1) is the move corresponding to the value max*)


(* Return the element of maximum value
 * in an array of couple (move, value) *)
let max_list_dir tab =
  let index = ref 0 in
  let max_value x y=
    let (a,b) = x and (c,d)=y in
    if b>d then x else (index:= !index + 1; y)
  in
  let (m,max)= List.fold_left max_value ((Move.E, 0),-1) tab in
  (m,max)


(*Function  which return the best move to do from position p*)
let penguin_best_choice map p fct_max=
  let aux d =
    try
      best_of_dir map p fct_max d
    with
    | Pascettedirection -> ((d,0),-1)
  in
  let l = List.map aux Move.all_directions in
  let (m_max, value)= max_list_dir l in
  if value = -1 then raise Peutpasbouger;
  (m_max, value)
(* l'exception Peutpasbouger se lève si le manchot ne peut pas bouger, ce qui n'est pas censé arriver pour l'instant*)


(* Choose the penguin with the best possible move in a list*)
(*the value of a move is given by fct_max*)
let best_penguin map p_list fct_max =
  let best_m = ref (Move.E,0) and best_v = ref (-1) and best_p = ref (-1, -1) in
  let aux p =
    try
      let (m,v) =penguin_best_choice map p fct_max in
      if v> !best_v then (best_v:= v; best_m := m; best_p :=p)
    with
    | Peutpasbouger -> ()
  in
  List.iter aux p_list;
  if (!best_p = (-1,-1)) then raise (FatalError "Ce joueur ne peut pas jouer!");
  (!best_p, !best_m)


(*current ai*)
let ai map p_list =
  (best_penguin  map p_list (nb_poissons map))


(*------------------------------------------Second AI -------------------------------------*)

(*This AI only consider the adjacent cases*)


let penguin_best_choice2 map p fct_max=
  let aux d =
     let (b,p',_) = Move.legal_move_n map p (d,1) in
     if b then ((d,1),(fct_max p'))
     else ((d,0),-1)
  in
  let l = List.map aux Move.all_directions in
  let (m_max, value)= max_list_dir l in
  if value = -1 then raise Peutpasbouger;
  (m_max, value)


(* choose the best penguin for the second AI*)
let best_penguin2 map p_list fct_max =
  let best_m = ref (Move.E,0) and best_v = ref (-1) and best_p = ref (-1, -1) in
  let aux p =
    try
      let (m,v) =penguin_best_choice2 map p fct_max in
      if v> !best_v then (best_v:= v; best_m := m; best_p :=p)
    with
    | Peutpasbouger -> ()
  in
  List.iter aux p_list;
  if (!best_p = (-1,-1)) then raise (FatalError "Ce joueur ne peut pas jouer!");
  (!best_p, !best_m)


(*current ai*)
let ai2 map p_list =
  (best_penguin2  map p_list (nb_poissons map))



(*-------------------------------------Random AI-------------------------------*)

(*Compute the possible move for a penguin*)
let all_possible map p =
  let l = ref [] in
  let existe = ref false in
  let rec aux d l n=
    if l=[] then []
    else (p,(d,n))::(aux d (List.tl(l)) (n+1))
  in
  let aux2 d = 
    let acc =  ref [] in
    let justforthetry =
      try
        acc := (accessible map p d)
      with
      | Pascettedirection ->(acc:= [])
    in
    justforthetry;
    let temp = (aux d (!acc) 1) in
    if temp<>[] then existe:=true;
    l:= temp@(!l)
  in
  List.iter aux2 Move.all_directions;
  !l

(*Randomly choose among possibility*)
let ai3 map p_list =
   let l = ref [] in
   let aux p =
     l:= (all_possible map p)@(!l)
   in
   List.iter aux p_list;
   let size = (List.length !l) in
   let n = Random.int (List.length !l) in
   if size=0 then raise (FatalError "Ce joueur ne peut pas jouer!");
   (List.nth (!l) n)








